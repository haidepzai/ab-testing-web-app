import { jStat } from "jstat";
import i18n from "../i18n";

export function performABTest(
  visitorsA,
  conversionsA,
  visitorsB,
  conversionsB,
  hypothesis,
  confidence
) {
  // Validate input types and ranges
  [visitorsA, conversionsA, visitorsB, conversionsB].forEach((value) => {
    if (typeof value !== "number" || isNaN(value) || value < 0) {
      console.error("All input values must be non-negative numbers.");
      throw new Error("Invalid input values.");
    }
  });

  if (conversionsA > visitorsA || conversionsB > visitorsB) {
    console.error("Conversions cannot exceed visitors.");
    throw new Error("Conversions exceed visitors.");
  }

  const rateA = conversionsA / visitorsA;
  const rateB = conversionsB / visitorsB;

  console.log(`Rates calculated: RateA = ${rateA}, RateB = ${rateB}`);

  // Calculate standard errors
  const seA = Math.sqrt((rateA * (1 - rateA)) / visitorsA);
  const seB = Math.sqrt((rateB * (1 - rateB)) / visitorsB);

  console.log(`Standard Errors calculated: SE A = ${seA}, SE B = ${seB}`);

  if (isNaN(seA) || isNaN(seB)) {
    console.error(
      "Standard error calculation failed due to invalid rate or sample size."
    );
    return { significant: false, chartData: null, z: NaN, zCritical: NaN };
  }

  const seDifference = Math.sqrt(seA ** 2 + seB ** 2);

  if (seDifference === 0) {
    console.warn(
      "Standard Error of difference is zero, Z-score cannot be calculated."
    );
    return { significant: false, chartData: null, z: NaN, zCritical: NaN };
  }

  const z = (rateB - rateA) / seDifference;

  let zCritical;
  if (confidence === 90) {
    zCritical = hypothesis === "one-sided" ? 1.28 : 1.645;
  } else if (confidence === 95) {
    zCritical = hypothesis === "one-sided" ? 1.645 : 1.96;
  } else if (confidence === 99) {
    zCritical = hypothesis === "one-sided" ? 2.33 : 2.576;
  } else {
    zCritical = 1.96; // Default to 95% two-sided
  }

  const significant =
    hypothesis === "one-sided" ? z > zCritical : Math.abs(z) > zCritical;

  // Calculate relative uplift
  const relativeUplift = ((rateB - rateA) / rateA) * 100;

  // Calculate SRM
  const totalVisitors = visitorsA + visitorsB;
  const expectedA = totalVisitors / 2;
  const expectedB = totalVisitors / 2;
  const srm =
    Math.abs(visitorsA - expectedA) > 0.05 * expectedA ||
    Math.abs(visitorsB - expectedB) > 0.05 * expectedB;

  const chartData = {
    labels: [i18n.t("BAR_CHART_TITLE")],
    datasets: [
      {
        label: i18n.t('SITE_CONVERSION_RATE', { site: "A", rate: (rateA * 100).toFixed(2) }),
        data: [rateA * 100],
        backgroundColor: "rgba(75, 192, 192, 0.2)",
        borderColor: "rgba(75, 192, 192, 1)",
        borderWidth: 1,
      },
      {
        label: i18n.t('SITE_CONVERSION_RATE', { site: "B", rate: (rateB * 100).toFixed(2) }),
        data: [rateB * 100],
        backgroundColor: "rgba(153, 102, 255, 0.2)",
        borderColor: "rgba(153, 102, 255, 1)",
        borderWidth: 1,
      },
    ],
  };

  // Calculate p-value
  let pValue;
  if (hypothesis === "one-sided") {
    pValue = 1 - jStat.normal.cdf(z, 0, 1);
  } else {
    pValue = 2 * (1 - jStat.normal.cdf(Math.abs(z), 0, 1));
  }

  return {
    significant,
    chartData,
    z,
    zCritical,
    rateA,
    rateB,
    seA,
    seB,
    seDifference,
    visitorsA,
    visitorsB,
    relativeUplift,
    pValue,
    srm,
  };
}
