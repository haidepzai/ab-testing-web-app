import { jStat } from "jstat";

export function performPreTestAnalysis(
  visitors,
  conversions,
  uplift,
  hypothesis,
  confidence
) {
  const rateA = conversions / visitors;
  const rateB = rateA * (1 + uplift);
  const seA = Math.sqrt((rateA * (1 - rateA)) / visitors);
  const seB = Math.sqrt((rateB * (1 - rateB)) / visitors);
  const seDifference = Math.sqrt(seA ** 2 + seB ** 2);
  const z = (rateB - rateA) / seDifference;

  let zCritical;
  if (confidence === 90) {
    zCritical = hypothesis === "one-sided" ? 1.28 : 1.645;
  } else if (confidence === 95) {
    zCritical = hypothesis === "one-sided" ? 1.645 : 1.96;
  } else if (confidence === 99) {
    zCritical = hypothesis === "one-sided" ? 2.33 : 2.576;
  } else {
    zCritical = 1.96; // Default to 95% two-sided
  }

  const significant =
    hypothesis === "one-sided" ? z > zCritical : Math.abs(z) > zCritical;

  const pValue =
    hypothesis === "one-sided"
      ? 1 - jStat.normal.cdf(z, 0, 1)
      : 2 * (1 - jStat.normal.cdf(Math.abs(z), 0, 1));

  return {
    rateA,
    rateB,
    seA,
    seB,
    seDifference,
    z,
    zCritical,
    pValue,
    significant,
    uplift,
    confidence,
  };
}
