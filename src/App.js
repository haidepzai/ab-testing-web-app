import React, { useState } from "react";
import {
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { ABTestProvider } from "./context/ABTestContext";
import ABTestForm from "./components/ABTestForm";
import ABTestResult from "./components/ABTestResult";
import PreTestAnalysisForm from "./components/PreTestAnalysisForm";
import GoalConversionForm from "./components/GoalConversionForm"; // Import the new component
import { useTranslation } from "react-i18next";
import "./App.css";

function App() {
  const { t } = useTranslation();
  const [mode, setMode] = useState("testEvaluation");

  const handleModeChange = (event) => {
    setMode(event.target.value);
  };

  return (
    <ABTestProvider>
      <Container maxWidth="md" style={{ marginBottom: "2rem", marginTop: "2rem" }}>
        <FormControl fullWidth>
          <InputLabel>{t("SELECT_MODE")}</InputLabel>
          <Select value={mode} onChange={handleModeChange}>
            <MenuItem value="testEvaluation">{t("TEST_EVALUATION")}</MenuItem>
            <MenuItem value="preTestAnalysis">{t("PRE-TEST_ANALYSIS")}</MenuItem>
            <MenuItem value="goalConversion">{t("GOAL_CONVERSIONS")}</MenuItem>
          </Select>
        </FormControl>
        {mode === "testEvaluation" && (
          <>
            <ABTestForm />
            <ABTestResult />
          </>
        )}
        {mode === "preTestAnalysis" && <PreTestAnalysisForm />}
        {mode === "goalConversion" && <GoalConversionForm />} 
      </Container>
    </ABTestProvider>
  );
}

export default App;
