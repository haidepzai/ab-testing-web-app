import { useReducer, useEffect } from "react";
import {
  initialPreTestState,
  preTestAnalysisReducer,
} from "../context/preTestAnalysisReducer";
import { performPreTestAnalysis } from "../utils/preTestUtils";

const usePreTestAnalysisForm = () => {
  const [state, dispatch] = useReducer(
    preTestAnalysisReducer,
    initialPreTestState
  );

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({ type: "SET_IS_SUBMITTED", payload: true });
  };

  const handleChange = (type) => (e) => {
    dispatch({ type, payload: e.target.value });
  };

  useEffect(() => {
    if (!state.isSubmitted) return;

    const visitors = parseInt(state.uniqueVisitors);
    const conversions = parseInt(state.conversionsControl);
    const uplift = parseFloat(state.expectedUplift) / 100;

    if (isNaN(visitors) || isNaN(conversions) || isNaN(uplift)) {
      alert("Please enter valid numbers");
      return;
    }

    const result = performPreTestAnalysis(
      visitors,
      conversions,
      uplift,
      state.hypothesis,
      state.confidence
    );

    dispatch({
      type: "SET_RESULT",
      payload: result,
    });
  }, [
    state.uniqueVisitors,
    state.conversionsControl,
    state.expectedUplift,
    state.hypothesis,
    state.confidence,
    state.isSubmitted,
  ]);

  return {
    state,
    handleSubmit,
    handleChange,
  };
};

export default usePreTestAnalysisForm;
