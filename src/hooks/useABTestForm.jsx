import { useABTest } from '../context/ABTestContext';

const useABTestForm = () => {
  const { state, dispatch, handleSubmit, handleHypothesisChange, handleConfidenceChange } = useABTest();

  const handleChange = (type) => (e) => {
    dispatch({ type, payload: e.target.value });
  };

  return {
    siteAVisitors: state.siteAVisitors,
    setSiteAVisitors: handleChange("SET_SITE_A_VISITORS"),
    siteAConversions: state.siteAConversions,
    setSiteAConversions: handleChange("SET_SITE_A_CONVERSIONS"),
    siteBVisitors: state.siteBVisitors,
    setSiteBVisitors: handleChange("SET_SITE_B_VISITORS"),
    siteBConversions: state.siteBConversions,
    setSiteBConversions: handleChange("SET_SITE_B_CONVERSIONS"),
    hypothesis: state.hypothesis,
    setHypothesis: handleHypothesisChange,
    confidence: state.confidence,
    setConfidence: handleConfidenceChange,
    handleSubmit,
  };
};

export default useABTestForm;
