import React, { useState } from "react";
import {
  Container,
  TextField,
  Button,
  Typography,
  Paper,
  Box,
} from "@mui/material";
import { useTranslation } from "react-i18next";

const GoalConversionForm = () => {  
  const [goalVisitors, setGoalVisitors] = useState("");
  const [goalConversionRate, setGoalConversionRate] = useState("");
  const [goalConversions, setGoalConversions] = useState(null);

  const { t } = useTranslation();

  const handleGoalSubmit = (e) => {
    e.preventDefault();
    const visitors = parseInt(goalVisitors);
    const conversionRate = parseFloat(goalConversionRate) / 100;

    if (isNaN(visitors) || isNaN(conversionRate)) {
      alert(t("VALID_NUMBERS"));
      return;
    }

    const requiredConversions = Math.ceil(visitors * conversionRate);
    setGoalConversions(requiredConversions);
  };

  return (
    <Container maxWidth="md" style={{ marginTop: "2rem" }}>
      <Typography variant="h4" gutterBottom align="center">
        {t("GOAL_CONVERSIONS")}
      </Typography>
      <Paper elevation={3} style={{ padding: "2rem", marginBottom: "2rem" }}>
        <Box component="form" onSubmit={handleGoalSubmit} noValidate>
          <TextField
            label={t("ENTER_VISITORS")}
            type="number"
            value={goalVisitors}
            onChange={(e) => setGoalVisitors(e.target.value)}
            fullWidth
            margin="normal"
            required
          />
          <TextField
            label={t("ENTER_GOAL_CR")}
            type="number"
            value={goalConversionRate}
            onChange={(e) => setGoalConversionRate(e.target.value)}
            fullWidth
            margin="normal"
            required
          />
          <Button
            variant="contained"
            color="primary"
            type="submit"
            fullWidth
            style={{ marginTop: "1rem" }}
          >
            {t("CALCULATE")}
          </Button>
        </Box>
        {goalConversions !== null && (
          <Typography variant="body1" style={{ marginTop: "1rem" }}>
            {t("REQUIRED_CONVERSIONS")}: {goalConversions}
          </Typography>
        )}
      </Paper>
    </Container>
  );
};

export default GoalConversionForm;
