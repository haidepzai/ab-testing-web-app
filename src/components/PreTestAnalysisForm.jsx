import React from "react";
import {
  Container,
  TextField,
  Button,
  Typography,
  Paper,
  Box,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Grid,
} from "@mui/material";
import styles from "../styles/PreTestAnalysisForm.module.css";
import { useTranslation } from "react-i18next";
import usePreTestAnalysisForm from "../hooks/usePreTestAnalysisForm";

const PreTestAnalysisForm = () => {
  const { t } = useTranslation();
  const { state, handleSubmit, handleChange } = usePreTestAnalysisForm();

  const resultBoxClass = `${styles.resultBoxStyle} ${
    state.result?.significant ? styles.significant : styles["not-significant"]
  }`;

  return (
    <Container maxWidth="md" style={{ marginTop: "2rem" }}>
      <Typography variant="h4" gutterBottom align="center">
        {t("PRE-TEST_ANALYSIS")}
      </Typography>
      <Paper elevation={3} style={{ padding: "2rem" }}>
        <Box component="form" onSubmit={handleSubmit} noValidate>
          <TextField
            label={t("VISITORS_A")}
            type="number"
            value={state.uniqueVisitors}
            onChange={handleChange("SET_UNIQUE_VISITORS")}
            fullWidth
            margin="normal"
            required
          />
          <TextField
            label={t("CONVERSIONS_A")}
            type="number"
            value={state.conversionsControl}
            onChange={handleChange("SET_CONVERSIONS_CONTROL")}
            fullWidth
            margin="normal"
            required
          />
          <TextField
            label={t("EXPECTED_UPLIFT")}
            type="number"
            value={state.expectedUplift}
            onChange={handleChange("SET_EXPECTED_UPLIFT")}
            fullWidth
            margin="normal"
            required
          />
          <Grid
            container
            spacing={2}
            alignItems="center"
            style={{ marginTop: "1rem" }}
          >
            <Grid item xs={12} md={6}>
              <FormControl component="fieldset">
                <FormLabel component="legend">{t("HYPOTHESIS")}</FormLabel>
                <RadioGroup
                  row
                  value={state.hypothesis}
                  onChange={handleChange("SET_HYPOTHESIS")}
                >
                  <FormControlLabel
                    value="one-sided"
                    control={<Radio />}
                    label={t("ONE_SIDED")}
                  />
                  <FormControlLabel
                    value="two-sided"
                    control={<Radio />}
                    label={t("TWO_SIDED")}
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6} style={{ textAlign: "right" }}>
              <FormControl component="fieldset">
                <FormLabel component="legend">
                  {t("CONFIDENCE_LEVEL")}
                </FormLabel>
                <RadioGroup
                  row
                  value={state.confidence}
                  onChange={handleChange("SET_CONFIDENCE")}
                >
                  <FormControlLabel
                    value={90}
                    control={<Radio />}
                    label="90%"
                  />
                  <FormControlLabel
                    value={95}
                    control={<Radio />}
                    label="95%"
                  />
                  <FormControlLabel
                    value={99}
                    control={<Radio />}
                    label="99%"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
          </Grid>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            fullWidth
            style={{ marginTop: "1rem" }}
          >
            {t("CALCULATE")}
          </Button>
        </Box>
      </Paper>
      {state.result && (
        <Paper
          elevation={3}
          style={{ padding: "2rem", marginTop: "2rem" }}
          className={resultBoxClass}
        >
          <Typography variant="h6" gutterBottom>
            {t("PRE-TEST_RESULT")}
          </Typography>
          <Typography variant="body1">
            {state.result.significant
              ? t("VARIATION_COMPARISON", {
                  rateB: (state.result.rateB * 100).toFixed(2),
                  rateA: (state.result.rateA * 100).toFixed(2),
                  relativeUplift: (state.result.uplift * 100).toFixed(2),
                  confidence: state.result.confidence,
                })
              : t("INSUFFICIENT_DIFFERENCE", {
                  relativeUplift: (state.result.uplift * 100).toFixed(2),
                })}
          </Typography>
        </Paper>
      )}
    </Container>
  );
};

export default PreTestAnalysisForm;
