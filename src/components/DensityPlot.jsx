import React from "react";
import Plot from "react-plotly.js";
import { useABTest } from "../context/ABTestContext";
import { useTranslation } from "react-i18next";

const DensityPlot = () => {
  const { state } = useABTest();
  const { result, confidence, hypothesis } = state;

  const { t } = useTranslation();

  if (!result) return null;

  const { rateA, rateB, seDifference, zCritical, visitorsA, visitorsB } =
    result;

  // Calculate the position of the confidence level lines
  const confidenceLevelLineLower = rateA - zCritical * seDifference;
  const confidenceLevelLineUpper = rateA + zCritical * seDifference;

  // Define x-axis values for the plot, centered around the means
  const xMin = Math.min(rateA, rateB) - 4 * seDifference;
  const xMax = Math.max(rateA, rateB) + 4 * seDifference;
  const xValues = Array.from(
    { length: 1000 },
    (_, i) => xMin + (i / 999) * (xMax - xMin)
  );

  // Define the normal distribution function, scaled to visitors
  const normalDist = (x, mean, sd, scale) => {
    const expPart = Math.exp(-((x - mean) ** 2) / (2 * sd ** 2));
    return (scale / (sd * Math.sqrt(2 * Math.PI))) * expPart;
  };

  // Scale factor to align the peak with the number of visitors
  const scaleFactorA = visitorsA * seDifference * Math.sqrt(2 * Math.PI);
  const scaleFactorB = visitorsB * seDifference * Math.sqrt(2 * Math.PI);

  // Generate y-values for both distributions
  const yValuesA = xValues.map((x) =>
    normalDist(x, rateA, seDifference, scaleFactorA)
  );
  const yValuesB = xValues.map((x) =>
    normalDist(x, rateB, seDifference, scaleFactorB)
  );

  // Determine max Y value based on the number of visitors
  const maxVisitors = Math.max(visitorsA, visitorsB);
  const maxYValue = Math.ceil((maxVisitors * 1.1) / 10000) * 10000;

  // Plotly trace for site A
  const traceA = {
    x: xValues,
    y: yValuesA,
    type: "scatter",
    mode: "lines",
    name: `CRA: ${(rateA * 100).toFixed(2)}%`,
    line: { color: "gray" },
    fill: "tozeroy",
  };

  // Plotly trace for site B
  const traceB = {
    x: xValues,
    y: yValuesB,
    type: "scatter",
    mode: "lines",
    name: `CRB: ${(rateB * 100).toFixed(2)}%`,
    line: { color: "green" },
    fill: "tozeroy",
  };

  // Create the plot
  return (
    <Plot
      data={[traceA, traceB]}
      layout={{
        title: t("CONVERSION_RATE_DENSITY_PLOT"),
        xaxis: { title: t("CONVERSION_RATE"), range: [xMin, xMax] },
        yaxis: { title: t("DENSITY"), range: [0, maxYValue] },
        shapes: [
          // Highlight the critical region
          {
            type: "line",
            x0: rateA,
            y0: 0,
            x1: rateA,
            y1: Math.max(...yValuesA),
            line: { color: "gray", dash: "dot" },
          },
          {
            type: "line",
            x0: rateB,
            y0: 0,
            x1: rateB,
            y1: Math.max(...yValuesB),
            line: { color: "green", dash: "dot" },
          },
          {
            type: "line",
            x0: confidenceLevelLineUpper,
            y0: 0,
            x1: confidenceLevelLineUpper,
            y1: Math.max(...yValuesA),
            line: { color: "black", dash: "dash" },
          },
          ...(hypothesis === "two-sided"
            ? [
                {
                  type: "line",
                  x0: confidenceLevelLineLower,
                  y0: 0,
                  x1: confidenceLevelLineLower,
                  y1: Math.max(...yValuesA),
                  line: { color: "black", dash: "dash" },
                },
              ]
            : []),
        ],
        annotations: [
          {
            x: rateA,
            y: Math.max(...yValuesA),
            xref: "x",
            yref: "y",
            text: `CRA: ${(rateA * 100).toFixed(2)}%`,
            showarrow: true,
            arrowhead: 7,
            ax: 0,
            ay: -40,
          },
          {
            x: rateB,
            y: Math.max(...yValuesB),
            xref: "x",
            yref: "y",
            text: `CRB: ${(rateB * 100).toFixed(2)}%`,
            showarrow: true,
            arrowhead: 7,
            ax: 0,
            ay: -40,
          },
          {
            x: confidenceLevelLineUpper,
            y: Math.max(...yValuesA) / 2,
            xref: "x",
            yref: "y",
            text: `${confidence}%`,
            showarrow: true,
            arrowhead: 7,
            ax: 0,
            ay: -20,
          },
          ...(hypothesis === "two-sided"
            ? [
                {
                  x: confidenceLevelLineLower,
                  y: Math.max(...yValuesA) / 2,
                  xref: "x",
                  yref: "y",
                  text: `${confidence}%`,
                  showarrow: true,
                  arrowhead: 7,
                  ax: 0,
                  ay: -20,
                },
              ]
            : []),
        ],
      }}
      style={{ width: "100%", height: "100%" }}
    />
  );
};

export default DensityPlot;
