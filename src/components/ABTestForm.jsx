import React from "react";
import {
  Container,
  TextField,
  Button,
  Typography,
  Box,
  Grid,
  Paper,
  Radio,
  RadioGroup,
  FormControl,
  FormControlLabel,
  FormLabel,
} from "@mui/material";
import useABTestForm from "../hooks/useABTestForm";
import { useTranslation } from "react-i18next";

const ABTestForm = () => {
  const { t } = useTranslation();

  const {
    siteAVisitors,
    setSiteAVisitors,
    siteAConversions,
    setSiteAConversions,
    siteBVisitors,
    setSiteBVisitors,
    siteBConversions,
    setSiteBConversions,
    hypothesis,
    setHypothesis,
    confidence,
    setConfidence,
    handleSubmit,
  } = useABTestForm();

  return (
    <Container maxWidth="md" style={{ marginTop: "2rem" }}>
      <Typography variant="h4" gutterBottom align="center">
        {t("TITLE")}
      </Typography>
      <Paper elevation={3} style={{ padding: "2rem" }}>
        <Box component="form" onSubmit={handleSubmit} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <TextField
                label={t("VISITORS_A")}
                type="number"
                value={siteAVisitors}
                onChange={setSiteAVisitors}
                fullWidth
                margin="normal"
                required
              />
              <TextField
                label={t("CONVERSIONS_A")}
                type="number"
                value={siteAConversions}
                onChange={setSiteAConversions}
                fullWidth
                margin="normal"
                required
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                label={t("VISITORS_B")}
                type="number"
                value={siteBVisitors}
                onChange={setSiteBVisitors}
                fullWidth
                margin="normal"
                required
              />
              <TextField
                label={t("CONVERSIONS_B")}
                type="number"
                value={siteBConversions}
                onChange={setSiteBConversions}
                fullWidth
                margin="normal"
                required
              />
            </Grid>
          </Grid>
          <Grid
            container
            spacing={2}
            alignItems="center"
            style={{ marginTop: "1rem" }}
          >
            <Grid item xs={12} md={6}>
              <FormControl component="fieldset">
                <FormLabel component="legend">{t("HYPOTHESIS")}</FormLabel>
                <RadioGroup
                  row
                  value={hypothesis}
                  onChange={(e) => setHypothesis(e.target.value)}
                >
                  <FormControlLabel
                    value="one-sided"
                    control={<Radio />}
                    label={t("ONE_SIDED")}
                  />
                  <FormControlLabel
                    value="two-sided"
                    control={<Radio />}
                    label={t("TWO_SIDED")}
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6} style={{ textAlign: "right" }}>
              <FormControl component="fieldset">
                <FormLabel component="legend">{t("CONFIDENCE_LEVEL")}</FormLabel>
                <RadioGroup
                  row
                  value={confidence}
                  onChange={(e) => setConfidence(parseInt(e.target.value))}
                >
                  <FormControlLabel
                    value={90}
                    control={<Radio />}
                    label="90%"
                  />
                  <FormControlLabel
                    value={95}
                    control={<Radio />}
                    label="95%"
                  />
                  <FormControlLabel
                    value={99}
                    control={<Radio />}
                    label="99%"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
          </Grid>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            fullWidth
            style={{ marginTop: "1rem" }}
          >
            {t("PERFORM_TEST")}
          </Button>
        </Box>
      </Paper>
    </Container>
  );
};

export default ABTestForm;
