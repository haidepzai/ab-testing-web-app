import React, { useState } from "react";
import { Box, Typography, Paper, Container, Tabs, Tab } from "@mui/material";
import { Bar } from "react-chartjs-2";
import { useABTest } from "../context/ABTestContext";
import DensityPlot from "./DensityPlot";
import styles from "../styles/ABTestResult.module.css";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { useTranslation } from "react-i18next";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const ABTestResult = () => {
  const { state } = useABTest();
  const [tabValue, setTabValue] = useState(0);

  const { t } = useTranslation();

  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };

  if (!state.result) return null;

  const resultBoxClass = `${styles.resultBoxStyle} ${
    state.result.significant ? "" : styles["not-significant"]
  }`;

  return (
    <Container maxWidth="md" style={{ marginTop: "2rem" }}>
      <Box mt={4}>
        <Paper elevation={3} style={{ padding: "2rem" }}>
          <Box className={resultBoxClass}>
            <Typography variant="h6" gutterBottom>
              {t("TEST_RESULT")}
            </Typography>
            <Typography variant="body1">
              {state.result.significant
                ? t("SIGNIFICANT_RESULT")
                : t("NOT_SIGNIFICANT_RESULT")}
            </Typography>
            {state.result.z !== undefined &&
              state.result.zCritical !== undefined &&
              !isNaN(state.result.z) &&
              !isNaN(state.result.zCritical) && (
                <Typography variant="body2">
                  {t("Z_SCORE")}: {state.result.z.toFixed(4)}, {t("Z_VALUE")}:{" "}
                  {state.result.zCritical.toFixed(4)}
                </Typography>
              )}
            {state.result.pValue !== undefined && (
              <Typography variant="body2">
                {t("P_VALUE")}: {state.result.pValue.toFixed(4)}
              </Typography>
            )}
            {state.result.rateA !== undefined && (
              <Typography variant="body2">
                {t("CONVERSION_RATE_A")}:{" "}
                {(state.result.rateA * 100).toFixed(2)}%
              </Typography>
            )}
            {state.result.rateB !== undefined && (
              <Typography variant="body2">
                {t("CONVERSION_RATE_A")}:{" "}
                {(state.result.rateB * 100).toFixed(2)}%
              </Typography>
            )}
            {state.result.seA !== undefined && (
              <Typography variant="body2">
                {t("ERROR_A")}: {state.result.seA.toFixed(6)}
              </Typography>
            )}
            {state.result.seB !== undefined && (
              <Typography variant="body2">
                {t("ERROR_B")}: {state.result.seB.toFixed(6)}
              </Typography>
            )}
            {state.result.seDifference !== undefined && (
              <Typography variant="body2">
                {t("ERROR_DIFFERENCE")}: {state.result.seDifference.toFixed(6)}
              </Typography>
            )}
            {state.result.relativeUplift !== undefined && (
              <Typography variant="body2">
                {t("RELATIVE_UPLIFT")}: {state.result.relativeUplift.toFixed(2)}
                %
              </Typography>
            )}
            {state.result.significant ? (
              <Typography variant="body2" style={{ marginTop: "1rem" }}>
                {t("VARIATION_COMPARISON", {
                  rateB: (state.result.rateB * 100).toFixed(2),
                  rateA: (state.result.rateA * 100).toFixed(2),
                  relativeUplift: state.result.relativeUplift.toFixed(2),
                  confidence: state.confidence,
                })}
              </Typography>
            ) : (
              <Typography variant="body2" style={{ marginTop: "1rem" }}>
                {t("INSUFFICIENT_DIFFERENCE", {
                  relativeUplift: state.result.relativeUplift.toFixed(2),
                })}
              </Typography>
            )}
          </Box>
          {state.result.srm && (
            <Box className={styles.alertBoxStyle}>
              <Typography variant="h6" gutterBottom>
                {t("SRM_ALERT")}:
              </Typography>
              <Typography variant="body2">{t("SRM_MESSAGE")}</Typography>
            </Box>
          )}
          <Tabs value={tabValue} onChange={handleTabChange} centered>
            <Tab label={t("BAR_CHART")} />
            <Tab label={t("DENSITY_PLOT")} />
          </Tabs>
          <Box mt={2}>
            {tabValue === 0 && state.result.chartData && (
              <Bar data={state.result.chartData} />
            )}
            {tabValue === 1 && <DensityPlot />}
          </Box>
        </Paper>
      </Box>
    </Container>
  );
};

export default ABTestResult;
