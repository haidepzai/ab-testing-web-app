export const initialPreTestState = {
  uniqueVisitors: "",
  conversionsControl: "",
  expectedUplift: "",
  hypothesis: "one-sided",
  confidence: 95,
  result: null,
  isSubmitted: false,
};

export const preTestAnalysisReducer = (state, action) => {
  switch (action.type) {
    case "SET_UNIQUE_VISITORS":
      return { ...state, uniqueVisitors: action.payload };
    case "SET_CONVERSIONS_CONTROL":
      return { ...state, conversionsControl: action.payload };
    case "SET_EXPECTED_UPLIFT":
      return { ...state, expectedUplift: action.payload };
    case "SET_HYPOTHESIS":
      return { ...state, hypothesis: action.payload };
    case "SET_CONFIDENCE":
      return { ...state, confidence: action.payload };
    case "SET_RESULT":
      return { ...state, result: action.payload };
    case "SET_IS_SUBMITTED":
      return { ...state, isSubmitted: action.payload };
    default:
      return state;
  }
};
