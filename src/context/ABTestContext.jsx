import React, { createContext, useContext, useReducer, useEffect } from "react";
import { performABTest } from "../utils/abTestUtils";
import { initialState, reducer } from "./abTestReducer";
import { useTranslation } from "react-i18next";

const ABTestContext = createContext();

export const useABTest = () => {
  return useContext(ABTestContext);
};

export const ABTestProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const { t } = useTranslation();

  useEffect(() => {
    if (!state.isSubmitted) return;

    const visitorsA = parseInt(state.siteAVisitors);
    const conversionsA = parseInt(state.siteAConversions);
    const visitorsB = parseInt(state.siteBVisitors);
    const conversionsB = parseInt(state.siteBConversions);

    if (
      isNaN(visitorsA) || isNaN(conversionsA) ||
      isNaN(visitorsB) || isNaN(conversionsB) ||
      visitorsA <= 0 || visitorsB <= 0 ||
      conversionsA < 0 || conversionsB < 0 ||
      conversionsA > visitorsA || conversionsB > visitorsB
    ) {
      return;
    }

    const testResult = performABTest(
      visitorsA,
      conversionsA,
      visitorsB,
      conversionsB,
      state.hypothesis,
      state.confidence
    );
    dispatch({ type: "SET_RESULT", payload: testResult });
  }, [
    state.siteAVisitors,
    state.siteAConversions,
    state.siteBVisitors,
    state.siteBConversions,
    state.hypothesis,
    state.confidence,
    state.isSubmitted,
  ]);

  const handleSubmit = (e) => {
    e.preventDefault();

    const visitorsA = parseInt(state.siteAVisitors);
    const conversionsA = parseInt(state.siteAConversions);
    const visitorsB = parseInt(state.siteBVisitors);
    const conversionsB = parseInt(state.siteBConversions);

    if (
      isNaN(visitorsA) || isNaN(conversionsA) ||
      isNaN(visitorsB) || isNaN(conversionsB) ||
      visitorsA <= 0 || visitorsB <= 0 ||
      conversionsA < 0 || conversionsB < 0 ||
      conversionsA > visitorsA || conversionsB > visitorsB
    ) {
      alert(t("INVALID_NUMBERS"));
      return;
    }

    dispatch({ type: "SET_IS_SUBMITTED", payload: true });
  };

  const handleHypothesisChange = (newHypothesis) => {
    dispatch({ type: "SET_HYPOTHESIS", payload: newHypothesis });
  };

  const handleConfidenceChange = (newConfidence) => {
    dispatch({ type: "SET_CONFIDENCE", payload: newConfidence });
  };

  const value = {
    state,
    dispatch,
    handleSubmit,
    handleHypothesisChange,
    handleConfidenceChange,
  };

  return (
    <ABTestContext.Provider value={value}>{children}</ABTestContext.Provider>
  );
};
