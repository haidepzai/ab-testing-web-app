export const initialState = {
  siteAVisitors: "0",
  siteAConversions: "0",
  siteBVisitors: "0",
  siteBConversions: "0",
  hypothesis: "one-sided",
  confidence: 95,
  result: null,
  isSubmitted: false,
};

export const reducer = (state, action) => {
  switch (action.type) {
    case "SET_SITE_A_VISITORS":
      return { ...state, siteAVisitors: action.payload };
    case "SET_SITE_A_CONVERSIONS":
      return { ...state, siteAConversions: action.payload };
    case "SET_SITE_B_VISITORS":
      return { ...state, siteBVisitors: action.payload };
    case "SET_SITE_B_CONVERSIONS":
      return { ...state, siteBConversions: action.payload };
    case "SET_HYPOTHESIS":
      return { ...state, hypothesis: action.payload, isSubmitted: true };
    case "SET_CONFIDENCE":
      return { ...state, confidence: action.payload, isSubmitted: true };
    case "SET_RESULT":
      return { ...state, result: action.payload };
    case "SET_IS_SUBMITTED":
      return { ...state, isSubmitted: action.payload };
    default:
      return state;
  }
};
